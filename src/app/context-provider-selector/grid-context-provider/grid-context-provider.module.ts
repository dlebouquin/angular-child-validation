import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {GridContextProviderComponent} from './grid-context-provider.component';

@NgModule({
  declarations: [GridContextProviderComponent],
  exports: [GridContextProviderComponent],
  imports: [BrowserModule, NoopAnimationsModule, MatSelectModule, ReactiveFormsModule]
})
export class GridContextProviderModule {
}
