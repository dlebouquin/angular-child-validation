import {Component, forwardRef, Output} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

export interface GridContextProvider {
  name: string;
  level: string;
}

@Component({
  selector: 'app-grid-context-provider',
  templateUrl: './grid-context-provider.component.html',
  styleUrls: ['./grid-context-provider.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GridContextProviderComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => GridContextProviderComponent),
      multi: true
    }
  ]
})
export class GridContextProviderComponent implements ControlValueAccessor, Validator {

  @Output()
  gridContextProvider: GridContextProvider;

  names: string[] = [
    'INNOVATION',
    'WEBCHOC',
    'REINSURANCE',
    'PROMOTION',
    'RANGE',
    'MATURITY',
    'PURCHASED_S',
    'PURCHASED_SL',
    'CONSULTED_S',
    'CONSULTED_SL',
    'RATING',
    'PRIORITY'
  ];

  levels: string[] = ['H', 'M', 'L'];

  form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    level: new FormControl('', [Validators.required])
  });

  public onTouched: () => void = () => {
  };

  writeValue(val: any): void {
    if (val) {
      this.form.setValue(val, {emitEvent: false});
    }
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.form.disable() : this.form.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.form.valid ? null : {invalidForm: {valid: false, message: 'Grid fields are invalid'}};
  }
}
