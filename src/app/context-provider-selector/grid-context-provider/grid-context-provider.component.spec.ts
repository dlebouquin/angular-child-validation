import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GridContextProviderComponent} from './grid-context-provider.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule, MatSelectModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('GridContextProviderComponent', () => {
  let component: GridContextProviderComponent;
  let fixture: ComponentFixture<GridContextProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GridContextProviderComponent],
      imports: [BrowserModule, NoopAnimationsModule, ReactiveFormsModule, MatInputModule, MatSelectModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridContextProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid with an empty name', () => {
    component.form.get('name').setValue('');
    component.form.get('level').setValue(component.levels[0]);
    expect(component.form.invalid).toBeTruthy();
  });

  it('should be invalid with an empty level', () => {
    component.form.get('name').setValue(component.names[0]);
    component.form.get('level').setValue('');
    expect(component.form.invalid).toBeTruthy();
  });

  it('should be valid when name and level are set', () => {
    component.form.get('name').setValue(component.names[0]);
    component.form.get('level').setValue(component.levels[0]);
    expect(component.form.valid).toBeTruthy();
  });
});
