import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextProviderSelectorComponent } from './context-provider-selector.component';
import {ContextProviderSelectorModule} from './context-provider-selector.module';

describe('ContextProviderSelectorComponent', () => {
  let component: ContextProviderSelectorComponent;
  let fixture: ComponentFixture<ContextProviderSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ContextProviderSelectorModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextProviderSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid without context provider selected', () => {
    expect(component.form.invalid).toBeTruthy();
  });
});
