import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AutolevelContextProviderComponent} from './autolevel-context-provider.component';
import {AutolevelContextProviderModule} from './autolevel-context-provider.module';

describe('AutolevelContextProviderComponent', () => {
  let component: AutolevelContextProviderComponent;
  let fixture: ComponentFixture<AutolevelContextProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({imports: [AutolevelContextProviderModule]})
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutolevelContextProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
