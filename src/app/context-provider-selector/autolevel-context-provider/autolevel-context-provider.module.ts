import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material';
import {AutolevelContextProviderComponent} from './autolevel-context-provider.component';

@NgModule({
  declarations: [AutolevelContextProviderComponent],
  exports: [AutolevelContextProviderComponent],
  imports: [ BrowserModule, NoopAnimationsModule, ReactiveFormsModule, MatSelectModule]
})
export class AutolevelContextProviderModule {
}
