import {Component, forwardRef} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-autolevel-context-provider',
  templateUrl: './autolevel-context-provider.component.html',
  styleUrls: ['./autolevel-context-provider.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutolevelContextProviderComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => AutolevelContextProviderComponent),
      multi: true
    }
  ]
})
export class AutolevelContextProviderComponent implements ControlValueAccessor, Validator {
  types: string[] = ['order', 'selection', 'consultation'];
  restrictions: string[] = ['S', 'SL', 'OS', 'OOSL'];

  componentForm: FormGroup = new FormGroup({
    type: new FormControl('', [Validators.required]),
    restriction: new FormControl('', [Validators.required]),
    minRedundancy: new FormControl('', [Validators.required, Validators.min(0)]),
    maxSolution: new FormControl('', [Validators.required, Validators.min(0)])
  });

  public onTouched: () => void = () => {
  };

  writeValue(val: any): void {
    console.log('autolevel', val);
    if (val) {
      this.componentForm.setValue(val, {emitEvent: false});
    }
  }

  registerOnChange(fn: any): void {
    this.componentForm.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.componentForm.disable() : this.componentForm.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.componentForm.valid ? null : {invalidForm: {valid: false, message: 'AutoLevelAssociated fields are invalid'}};
  }
}
