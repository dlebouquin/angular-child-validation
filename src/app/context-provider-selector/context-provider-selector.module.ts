import {NgModule} from '@angular/core';
import {ContextProviderSelectorComponent} from './context-provider-selector.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {GridContextProviderModule} from './grid-context-provider/grid-context-provider.module';
import {AutolevelContextProviderModule} from './autolevel-context-provider/autolevel-context-provider.module';

@NgModule({
  declarations: [ContextProviderSelectorComponent],
  exports: [ContextProviderSelectorComponent],
  imports: [BrowserModule, NoopAnimationsModule, FormsModule, MatFormFieldModule, MatSelectModule, ReactiveFormsModule, MatInputModule,
    GridContextProviderModule, AutolevelContextProviderModule]
})
export class ContextProviderSelectorModule {
}
