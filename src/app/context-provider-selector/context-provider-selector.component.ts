import {AfterViewInit, Component, forwardRef, Input} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';
import {MatSelectChange} from '@angular/material';

@Component({
  selector: 'app-context-provider-selector',
  templateUrl: './context-provider-selector.component.html',
  styleUrls: ['./context-provider-selector.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContextProviderSelectorComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ContextProviderSelectorComponent),
      multi: true
    }
  ]
})
export class ContextProviderSelectorComponent implements AfterViewInit, ControlValueAccessor, Validator {

  @Input('name')
  initialName: string;

  names: string[] = ['grid', 'autolevel'];

  public form: FormGroup = new FormGroup({
    selectedContextProviderName: new FormControl('', [Validators.required]),
    selectedContextProvider: new FormControl()
  });

  ngAfterViewInit(): void {
    this.form.get('selectedContextProviderName').setValue(this.initialName);
  }

  public onTouched: () => void = () => {
  };

  writeValue(val: any): void {
    if (val) {
      this.form.setValue(val, {emitEvent: false});
    }
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.form.disable() : this.form.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.form.valid ? null : {invalidForm: {valid: false, message: 'Context provider selector fields are invalid'}};
  }

  onSelectionChange($event: MatSelectChange) {
    this.form.get('selectedContextProvider').reset();
  }
}
