import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewPublicationToolComponent} from './new-publication-tool.component';
import {NewPublicationToolModule} from './new-publication-tool.module';

fdescribe('NewPublicationToolComponent', () => {
  let component: NewPublicationToolComponent;
  let fixture: ComponentFixture<NewPublicationToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NewPublicationToolModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPublicationToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get a null value if no context provider selector is selected', () => {
    const selected = component.form.get('contextProvider').value;
    expect(selected).toBeNull();
  });

  it('should be invalid without context provider', () => {
    expect(component.form.invalid).toBeTruthy();
  });
});
