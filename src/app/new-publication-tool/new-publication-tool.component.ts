import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-new-publication-tool',
  templateUrl: './new-publication-tool.component.html',
  styleUrls: ['./new-publication-tool.component.css']
})
export class NewPublicationToolComponent implements OnInit, OnChanges {

  @Input()
  publicationTool: { contextProviderName: string };

  form: FormGroup = new FormGroup({
    contextProvider: new FormControl()
  });

  contextProviderName: string;

  ngOnInit(): void {
    console.log('onInit', this.publicationTool);
    // this.contextProviderName = this.publicationTool.contextProviderName;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', changes);
    if (changes['publicationTool']) {
      this.contextProviderName = changes['publicationTool'].currentValue.contextProviderName;
    }
  }
}
