import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewPublicationToolComponent } from './new-publication-tool.component';
import {ContextProviderSelectorModule} from '../context-provider-selector/context-provider-selector.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [NewPublicationToolComponent],
  exports: [
    NewPublicationToolComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ContextProviderSelectorModule
  ]
})
export class NewPublicationToolModule { }
