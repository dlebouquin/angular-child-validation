import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContextProviderSelectorComponent} from './context-provider-selector/context-provider-selector.component';

const routes: Routes = [
  {
    path: 'selector',
    pathMatch: 'full',
    component: ContextProviderSelectorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
