import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NewPublicationToolModule} from './new-publication-tool/new-publication-tool.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NewPublicationToolModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
